<?php

/**
 * @file
 * Yild configuration UI.
 */

// Load yild actions.
module_load_include('inc', 'yild', 'yild.actions');

/**
 * Implements hook_admin_settings_form().
 */
function yild_admin_settings_form($form, &$form_state) {
  $form = array();

  $form['yild_global_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Yild configuration'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $vocabularies = taxonomy_get_vocabularies();
  $options = array();
  foreach ($vocabularies as $vocabulary) {
    if (_yild_is_yild_vocabulary($vocabulary->machine_name)) {
      $options[$vocabulary->machine_name] = $vocabulary->name;
    }
  }

  if (empty($options)) {
    $no_vocabularies = TRUE;
    $options[] = t('No convertable vocabularies found');
  }
  $form['yild_global_settings']['yild_vocabulary'] = array(
    '#type' => 'select',
    '#title' => t('Yild vocabulary'),
    '#description' => t('The vocabulary Yild tags will be stored in. Must be a Yild enabled vocabulary. You can convert a vocabulary to Yild on the <a href="@url">Yild tools page</a>.', ['@url' => url('/admin/structure/yild')]),
    '#default_value' => variable_get('yild_vocabulary', 'yild_tags'),
    '#options' => $options,
  );

  $form['yild_global_settings']['yild_result_language'] = array(
    '#type' => 'textfield',
    '#title' => t('Results language'),
    '#description' => t('The two letter language code for the results. Default is "en" for English.'),
    '#default_value' => variable_get('yild_result_language', 'en'),
    '#disabled' => FALSE,
    '#size' => 3,
    '#maxlength' => 2,
  );

  $form['yild_global_settings']['yild_show_progress_bar'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show tag progress bar'),
    '#description' => t('When enabled (default), a progress bar will be shown when tagging to indicate whether the suggested amount of terms have been entered.'),
    '#default_value' => variable_get('yild_show_progress_bar', "1"),
  );

  $form['yild_global_settings']['yild_match_terms'] = array(
    '#type' => 'checkbox',
    '#title' => t('Update terms with new data'),
    '#description' => t('When enabled, existing terms will be updated with the data received from the provider whenever a tag is reused. Default: disabled.'),
    '#default_value' => variable_get('yild_match_terms', "0"),
  );

  $form['yild_global_settings']['yild_progress_bar_amount'] = array(
    '#type' => 'select',
    '#title' => t('Minimum suggested term amount'),
    '#description' => t('The amount of terms that each yild field should ideally have.'),
    '#default_value' => variable_get('yild_progress_bar_amount', 8),
    '#options' => array(
      '1' => t('1 term'),
      '2' => t('2 terms'),
      '3' => t('3 terms'),
      '4' => t('4 terms'),
      '5' => t('5 terms'),
      '6' => t('6 terms'),
      '7' => t('7 terms'),
      '8' => t('8 terms'),
      '9' => t('9 terms'),
      '10' => t('10 terms'),
    ),
  );

  $form['yild_global_settings']['yild_show_provider'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show provider name in terms.'),
    '#description' => t('When enabled (default), each selected term will be shown with a prefix of the provider that it was fetched from. To save space you can disable this, especially if you use only one provider.'),
    '#default_value' => variable_get('yild_show_provider', "1"),
  );

  $form['yild_global_settings']['yild_expose_endpoint'] = array(
    '#type' => 'checkbox',
    '#title' => t('Expose Yild endpoint'),
    '#description' => t('When enabled (default), other sites running yild could ask you for information about your content tagged with a specific Yild term.'),
    '#default_value' => variable_get('yild_expose_endpoint', "1"),
  );

  if (count(module_implements('yild_improve_term_pages')) > 0) {
    $form['yild_global_settings']['yild_improve_term_pages'] = array(
      '#type' => 'checkbox',
      '#title' => t('Improved Yild term pages'),
      '#description' => t('When enabled, term pages will be improved by the provider modules when possible. This might mean term pages contain articles with related terms as well as the original term.'),
      '#default_value' => variable_get('yild_improve_term_pages', "0"),
    );
  }

  $form['yild_global_settings']['security_info'] = array(
    '#markup' => '<p class="warning">Please note that password and ip-list are treated as <strong>OR-conditions</strong>. That means you must either be on the IP-list <strong>OR</strong> have the correct password to be let through. You don\'t need both.</p>',
  );

  $form['yild_global_settings']['yild_api_password_salt'] = array(
    '#type' => 'textfield',
    '#title' => t('Yild API hash salt.'),
    '#description' => t('The salt to use for protecting API request. The final parameter of the API request should be the next to last parameter concatenated with this salt and run through MD5, so for the API request http://example.com/yild/api/provider/freebase, you should add a last parameter MD5("freebase" . $salt). Leave empty to have the API endpoint completely open, which could potentially be a point of entry for increasing your server load.'),
    '#default_value' => variable_get('yild_api_password_salt', ''),
    '#disabled' => FALSE,
    '#size' => 50,
    '#maxlength' => 50,
  );

  $form['yild_global_settings']['yild_api_ip_list'] = array(
    '#type' => 'textfield',
    '#title' => t('The list of IP:s allowed to access the Yild API endpoint.'),
    '#description' => t('A comma separated list of IP:s. If this field is non-empty, the requesting ip is matched against all valid ip:s in this list and only those request matching an ip will be let through.'),
    '#default_value' => variable_get('yild_api_ip_list', ''),
    '#disabled' => FALSE,
    '#size' => 200,
    '#maxlength' => 4096,
  );

  return system_settings_form($form);
}

/**
 * Defines the form for picking a vocabulary to convert to a yild vocabulary.
 */
function yild_convert_form($form, $form_state) {
  if (!empty($form_state['storage']['values'])) {
    // Return the confirmation form.
    return confirm_form(
      array(),
      t('Are you sure you want to convert the vocabulary?'),
      '/admin/structure/yild',
      t('This will convert the vocabulary <strong>@vocabulary</strong> to a yild vocabulary.', ['@vocabulary' => $form_state['storage']['values']['vocabulary']]),
      t('Yes'),
      t('Cancel')
    );
  }

  $vocabularies = taxonomy_get_vocabularies();
  $options = array();
  foreach ($vocabularies as $vocabulary) {
    if (!_yild_is_yild_vocabulary($vocabulary->machine_name)) {
      $options[$vocabulary->machine_name] = $vocabulary->name;
    }
  }

  if (empty($options)) {
    $no_vocabularies = TRUE;
    $options[] = t('No convertable vocabularies found');
  }

  $form = array();

  $form['yild_conversion'] = array(
    '#type' => 'fieldset',
    '#title' => t('Yild conversion'),
    '#description' => t('This will simply add four fields (external id, provider, disambiguator and data) to the vocabulary and not change the content in any way.'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );

  $form['yild_conversion']['vocabulary'] = array(
    '#type' => 'select',
    '#options' => $options,
    '#disabled' => empty($options),
    '#title' => t('Select a vocabulary to convert to a Yild vocabulary'),
  );

  $form['yild_conversion']['submit'] = array(
    '#type' => 'submit',
    '#disabled' => !empty($no_vocabularies),
    '#value' => t('Convert selected vocabulary'),
  );

  return $form;
}

/**
 * Implements hook_form_submit().
 */
function yild_convert_form_submit($form, &$form_state) {
  if (empty($form_state['storage']['values'])) {
    $form_state['rebuild'] = TRUE;
    $form_state['storage']['values'] = $form_state['values'];
  }
  else {
    $vocabulary = $form_state['storage']['values']['vocabulary'];
    // Call the functions to create needed fields in the selected vocabulary.
    _yild_create_id_field($vocabulary);
    _yild_create_provider_field($vocabulary);
    _yild_create_disambiguator_field($vocabulary);
    _yild_create_data_field($vocabulary);
    drupal_set_message(t('Converted @vocabulary to a yild vocabulary.', ['@vocabulary' => $vocabulary]));
  }
}

/**
 * Checks whether a vocabulary is a yild vocabulary or not.
 *
 * This is done by checking the vocabulary has all required fields:
 * id-field, provider-field, disambiguator field and data-field.
 *
 * @param string $vocabulary_machine_name
 *   The machine name of the vocabulary to check.
 *
 * @return bool
 *   True if vocabulary is a yild vocabulary. False if not.
 */
function _yild_is_yild_vocabulary($vocabulary_machine_name) {
  if (empty($vocabulary_machine_name)) {
    return FALSE;
  }
  $fields = [
    'id' => 'yild_ext_id',
    'provider' => 'yild_provider',
    'disambiguator' => 'yild_disambiguator',
    'data' => 'yild_data',
  ];
  foreach ($fields as $label => $name) {
    $field_name = variable_get('yild_' . $label . '_field_name', $name);
    if (empty(field_info_instance('taxonomy_term', $field_name, $vocabulary_machine_name))) {
      return FALSE;
    }
  }
  return TRUE;
}

/**
 * Defines the form for choosing a provider to check your current terms against.
 */
function yild_match_form() {
  $vocabulary = variable_get('yild_vocabulary', 'yild_tags');
  $form = array();
  $form['yild_match'] = array(
    '#type' => 'fieldset',
    '#title' => t('Yild match terms'),
    '#description' => t('This tool is for checking up your current terms against one of your installed Yild providers to try to match them. Only terms missing an external id will be matched. Using this tool, strings found matching the current term name exactly will be matched by adding the external id and a provider name.'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );

  $options = array();
  $hook = 'yild_get_provider_name';
  $providers = array();
  foreach (module_implements($hook) as $module) {
    $name = module_invoke($module, $hook);
    $options[$name] = $name;
  }

  $form['yild_match']['provider'] = array(
    '#type' => 'select',
    '#options' => $options,
    '#disabled' => empty($options),
    '#title' => t('Select a provider to check terms in the vocabulary "@vocabulary" against.', ['@vocabulary' => $vocabulary]),
  );

  $form['yild_match']['batch'] = array(
    '#type' => 'select',
    '#options' => [
      'all' => 'all',
      '1' => '1',
      '5' => '5',
      '10' => '10',
      '20' => '20',
      '50' => '50',
      '100' => '100',
      '250' => '250',
      '500' => '500',
    ],
    '#title' => t('Select how many terms to match.'),
  );

  $form['yild_match']['dryrun'] = array(
    '#type' => 'checkbox',
    '#title' => t('Report only - do not change data.'),
  );

  $form['yild_match']['submit'] = array(
    '#type' => 'submit',
    '#disabled' => empty($options),
    '#value' => t('Match terms'),
  );

  return $form;
}

/**
 * Implements hook_form_submit().
 */
function yild_match_form_submit($form, &$form_state) {
  // Whether this is a dry run and we should only report on the findings.
  $dry_run = $form_state['values']['dryrun'];

  // The provider to use.
  $provider = $form_state['values']['provider'];

  // Total amount of terms to match.
  $limit = $form_state['values']['batch'] == 'all' ? 0 : $form_state['values']['batch'];

  // The vocabulary name in use for Yild terms.
  $vocabulary = variable_get('yild_vocabulary', 'yild_tags');

  if (!empty($vocabulary) && !empty($provider)) {
    $terms = _yild_get_terms_without_extid($vocabulary, $limit);
    // Only start the batch process if there are actual terms to fix.
    if (count($terms) > 0) {
      drupal_set_message(t('Running a @runtype batch of @amount terms on <strong>@vocabulary</strong> against the provider <strong>@provider</strong>.', [
        '@runtype' => $dry_run ? 'dry run' : 'live',
        '@amount' => count($terms),
        '@vocabulary' => $vocabulary,
        '@provider' => $provider,
      ]));
      $operations = [];
      foreach ($terms as $term) {
        $operations[] = ['_yild_batch_match_term', [$term, $provider, $dry_run]];
      }
      $batch = [
        'title' => t('Matching terms in @vocabulary against @provider', ['@vocabulary' => $vocabulary, '@provider' => $provider]),
        'file' => drupal_get_path('module', 'yild') . '/yild.admin.inc',
        'operations' => $operations,
        'progress_message' => t('Matching terms: @current / @total. Estimated time left: @estimate.'),
        'error_message' => t('There was an error matching the terms.'),
        'finished' => '_yild_batch_match_finished',
      ];
      batch_set($batch);
    }
    else {
      drupal_set_message(t('No terms without external id found in @vocabulary', ['@vocabulary' => $vocabulary]), 'warning');
    }
  }
  else {
    drupal_set_message(t('Unable to match terms because one or several parameters are missing.'), 'warning');
  }
}

/**
 * Callback for batch process.
 */
function _yild_batch_match_term($term, $provider, $dry_run, &$context) {
  // Id field name.
  $id_field_name = variable_get('yild_id_field_name', 'yild_ext_id');
  $provider_field_name = variable_get('yild_provider_field_name', 'yild_provider');

  // Initialise the results arrays for matches and fails.
  foreach (['match', 'fail'] as $type) {
    if (empty($context['results'][$type])) {
      $context['results'][$type] = [];
    }
  }

  $match = _yild_match_term_against_provider($term, $provider);
  if (!empty($match) && !empty($match->{$id_field_name}[LANGUAGE_NONE][0]['value'])) {
    $context['message'] = t('FOUND a match for term @name against @provider', [
      '@name' => $match->name,
      '@provider' => $provider,
    ]);
    $context['results']['match'][] = [
      'name' => $match->name,
      'tid' => $match->tid,
      'extid' => $match->$id_field_name[LANGUAGE_NONE][0]['value'],
    ];
    if (!$dry_run) {
      $match->batch_mode = TRUE;
      field_attach_update('taxonomy_term', $match);
      entity_get_controller('taxonomy_term')->resetCache([$match->tid]);
    }
  }
  else {
    $context['message'] = t('NO match for term @name against @provider', [
      '@name' => $term->name,
      '@provider' => $provider,
    ]);
    $context['results']['fail'][] = [
      'name' => $term->name,
      'tid' => $term->tid,
    ];
  }
}

/**
 * Callback for finished batch progress.
 */
function _yild_batch_match_finished($success, $results, $operations) {
  if ($success) {
    $message = t('@count items processed. @match matches / @fail failed. You can find a csv listing of the results in your <a href="@logurl">Drupal log</a>.', [
      '@count' => count($results['match']) + count($results['fail']),
      '@match' => count($results['match']),
      '@fail' => count($results['fail']),
      '@logurl' => url('admin/reports/dblog'),
    ]);
    foreach (['match', 'fail'] as $type) {
      $log[] = ucfirst($type);
      $log[] = 'name;tid;extid';
      foreach ($results[$type] as $result) {
        $log[] = implode(';', $result);
      }
    }

    drupal_set_message($message);
    // Output the results array if devel module is available (and hence dpm).
    if (module_exists('devel')) {
      dpm($results);
    }
    watchdog('yild', implode('<br>', $log));
  }
  else {
    $error_operation = reset($operations);
    $message = t('An error occurred while processing %error_operation with arguments: @arguments', [
      '%error_operation' => $error_operation[0],
      '@arguments' => print_r($error_operation[1], TRUE),
    ]);
    drupal_set_message($message, 'error');
  }
}

/**
 * Fetches all terms from a specific vocabulary that have no external id.
 *
 * @param string $vocabulary
 *   The vocabulary machine name.
 * @param int $limit
 *   The limit to how many terms we want to fetch.
 *
 * @return array
 *   An array of Drupal taxonomy term objects that are missing external id.
 */
function _yild_get_terms_without_extid($vocabulary, $limit = 0, $offset = 0) {
  // Vocabulary.
  $vocabulary = taxonomy_vocabulary_machine_name_load($vocabulary);
  if (empty($vocabulary)) {
    return [];
  }

  // Id field name.
  $id_field_name = variable_get('yild_id_field_name', 'yild_ext_id');

  $query = db_select('taxonomy_term_data', 'td');
  $query->fields('td', ['name', 'tid']);
  $query->leftJoin('field_data_yild_ext_id', 'extid', 'extid.entity_id=td.tid');
  $query->condition('td.vid', $vocabulary->vid, '=');
  $query->where('CHAR_LENGTH(td.name) > :minlength', [':minlength' => 1]);
  $query->isNull('extid.yild_ext_id_value');
  if (!empty($limit) || !empty($offset)) {
    $query->range($offset, $limit);
  }
  $tids = [];
  $result = $query->execute();
  while ($record = $result->fetchAssoc()) {
    $tids[] = $record['tid'];
  }
  return taxonomy_term_load_multiple($tids);
}

/**
 * Runs a match against a selected provider for a list of terms.
 *
 * @param array $term
 *   An array of Drupal term objects.
 * @param string $provider
 *   The name of the provider to match against.
 *
 * @return obj $term
 *   The term object with the external id and provider added if found.
 */
function _yild_match_term_against_provider($term, $provider) {
  // Id field name and provider field name.
  $id_field_name = variable_get('yild_id_field_name', 'yild_ext_id');
  $provider_field_name = variable_get('yild_provider_field_name', 'yild_provider');
  $disambiguator_field_name = variable_get('yild_disambiguator_field_name', 'yild_disambiguator');
  // Strip hashtag from term names beginning with one.
  if (substr($term->name, 0, 1) === '#') {
    $term->name = substr($term->name, 1);
  }
  $term->name = trim($term->name);
  $results = module_invoke('yild_' . $provider, 'yild_search', $term->name, variable_get('yild_result_language', 'en'));
  foreach ($results as $ext_id => $term_data) {
    $db_name = mb_strtolower($term->name);
    $provider_name = mb_strtolower($term_data['name']);
    $provider_alias = mb_strtolower($term_data['alias']);
    if (strcasecmp($provider_name, $db_name) === 0 || strcasecmp($provider_alias, $db_name) === 0) {
      // We found a match!
      // $term->name = $term_data['name'];
      $term->$id_field_name = [LANGUAGE_NONE => [['value' => $term_data['provider_id']]]];
      $term->$provider_field_name = [LANGUAGE_NONE => [['value' => $provider]]];
      $term->$disambiguator_field_name = [LANGUAGE_NONE => [['value' => $term_data['disambiguator']]]];
      break;
    }
  }
  // Return the processed terms.
  return $term;
}
