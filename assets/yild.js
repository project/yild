/**
 * @file
 * Yild javascript functionality.
 */

(function ($) {
  Drupal.behaviors.yild = {
    attach: function (context) {
      if (context === document) {
        var id_parts;
        // Listen to the autosuggest button if one is present.
        $('.yild__button--autosuggest').unbind("click");
        $('.yild__button--autosuggest').click(function () {
          var btn = $(this);
          var autosuggest_container = btn.parents('.yild__autosuggestContainer');
          var tags_container = autosuggest_container.find('.yild__autosuggestSuggestionsContainer');
          var target_container = autosuggest_container.siblings('.js-yild-tags-container');
          var hidden_field = $(this).yildGetHiddenField();
          if (!$(this).hasClass('inprogress')) {
            $(this).addClass('inprogress ajax-progress ajax-progress-throbber');
            // Fetch the content of all textareas with the name "body".
            var textfields = [];
            $('input#edit-title').each(function () {
              textfields.push($(this).val());
            });
            if (BUE.instances.length > 0) {
              $.each(BUE.instances, function (index, editor_instance) {
                textfields.push(editor_instance.getContent());
              });
            }
            else {
              $('textarea').each(function () {
                if ($(this).attr('id').indexOf('body') !== -1 && $(this).val().length > 30) {
                  textfields.push($(this).val());
                }
              });
            }

            if (textfields.length > 0) {
              // Call the autosuggest url as a post ajax request to fetch term suggestions.
              autosuggest_container.find('div.yild__throbber').show();
              $.post(Drupal.settings.basePath + 'yild/autosuggest', {text_to_autosuggest: textfields.join("\n")}, function (data) {
                // Loop through all hits and place them as suggestions under their
                // respective provider fields.
                $.each(data, function (key, val) {
                  id_parts = key.split(':');
                  var termid = id_parts.join(':').trim();
                  if (target_container.find('li.yild__termButton[data-id="' + termid + '"]').length === 0) {
                    tags_container.addYildTag(key, val.name, val.disambiguator, val.description, val.data, val.frequency, val.providerlabel, true, hidden_field);
                  }
                });
                if (data.debug) {
                  $('.yild__debugContainer').remove();
                  $('.yild__fieldset').last().append('<div class="yild__debugContainer">' + data.debug.toString() + '</div>');
                }
                autosuggest_container.find('.yild__throbber').hide();
                autosuggest_container.find('.yild__autosuggestHeader').show();
              });
            }
            $(this).removeClass('inprogress');
          }
        });
      }

      $('input.yild__autocomplete').once(function () {
        var acfield = $(this);
        var tagscontainer = $(this).parents('.yild__fieldset').find('.js-yild-tags-container');
        var hidden_field = $(this).yildGetHiddenField();
        acfield.bind('autocompleteSelect', function (e) {
          var selected_value = $(this).val();
          var value_parts = selected_value.split('|');
          var id = value_parts[0];
          var label = value_parts[1];
          var disambiguator = '';
          if (value_parts.length >= 3) {
            disambiguator = value_parts[2];
          }

          var alt_id = '';
          var alt_disambiguator = '';

          // If id contains double parameters, make a second term-button.
          if (id.indexOf('+') !== -1 && id.indexOf(':') !== -1) {
            var id_provider_parts = id.split(':');
            var providers = id_provider_parts.shift();
            var ids = id_provider_parts.join(':');

            var provider_parts = providers.split('+');
            var id_parts = ids.split('+');
            var disambiguator_parts = disambiguator.split('+');
            if (disambiguator_parts.length === 2) {
              disambiguator = disambiguator_parts[1];
              alt_disambiguator = disambiguator_parts[0];
            }

            if (id_parts.length >= 2 && provider_parts.length >= 2) {
              alt_id = provider_parts[0] + ':' + id_parts[0];
              id = provider_parts[1] + ':' + id_parts[1];
            }
          }

          var data = '';
          if (value_parts.length >= 4) {
            data = value_parts[3];
          }
          if (tagscontainer.length > 0) {
            tagscontainer.addYildTag(id, label, disambiguator, null, data, null, null, false, hidden_field);
            if (alt_id) {
              // Add alternative terms to their own providers if we have
              // multiple fields using different providers.
              tagscontainer.yildAddTagToProviderField(alt_id, label, alt_disambiguator ? alt_disambiguator : disambiguator, null, hidden_field);
            }
            tagscontainer.yildMoveValues(hidden_field);
            tagscontainer.yildRefreshButtons();
            acfield.val('');
          }
          else {
            // Unable to find container.
          }
        });

        // If we landed on this page because of a form validation error, we need
        // to make term buttons out of all the terms described in the hidden
        // fields.
        if ($('input.error').length > 0) {
          tagscontainer.yildMakeButtonsFromHiddenField(hidden_field);
        }

        tagscontainer.yildRefreshButtons();
        tagscontainer.yildRefreshProgressBar();
      });

      // Check if jQuery-ui sortable is loaded.
      if ($.fn.sortable !== undefined) {
        $('.js-yild-tags-container').each(function () {
          var container = $(this);
          container.sortable({
            stop: function (event, ui) {
              container.yildMoveValues(container.yildGetHiddenField());
            },
            placeholder: 'yild__termButton--placeholder',
            forcePlaceholderSize: true
          });
        });
      }

      // Ask each yild field to move value to hidden field.
      $('.js-yild-tags-container').each(function () {
        $(this).yildMoveValues($(this).siblings('.js-yild-hidden-field'));
      });
    }
  };

 /*
  * Given a string of comma-separated values, return a Boolean of whether it contains needle
  *
  * @param {string} haystack - Input string of comma-separated values, may have space after the comma
  * @param {string} needle - Value to find
  * @return {bool} Whether haystack contains needle
   */
  $.fn.csvContainsValue = function (haystack, needle) {
    var values = haystack.split(',');
    for (var i = 0; i < values.length; i++) {
      var value = values[i].trim();
      if (value === needle) {
        return true;
      }
    }
  return false;
  };

  /**
   * Prototype for adding term buttons to a container.
   *
   * @param {int} id - Unique term ID.
   * @param {string} label - Term label.
   * @param {string} disambiguator - Term disambiguator that describes what type of concept it is.
   * @param {string} description - The term description.
   * @param {array} data - Various data associated with the concept.
   * @param {string} usage - How many times the term has been used.
   * @param {string} providerlabel - An alternative provider label.
   * @param {bool} inactive - Whether the term button should be rendered as inactive.
   * @param {element} hidden_field - The jQuery element for a hidden_field.
   */

  $.fn.addYildTag = function (id, label, disambiguator, description, data, usage, providerlabel, inactive, hidden_field) {
    // Find out whether this container is an autosuggest container or not.
    var asContainer = $(this).hasClass('yild__autosuggestSuggestionsContainer');
    label = label.trim();
    var displaylabel = label;
    disambiguator = disambiguator.trim();
    description = description || '';
    data = data || '';
    usage = usage || '';
    longdescription = image = '';
    if (!disambiguator && description.length <= 50) {
      disambiguator = description;
    }
    else if (disambiguator.length > 50) {
      longdescription = disambiguator;
      disambiguator = '';
    }
    else if (description.length > 50) {
      longdescription = description;
      description = '';
    }
    data = data.split(';');
    var dataItems = [];
    $.each(data, function(idx, dataItem) {
      var dataParts = dataItem.split(':');
      dataItems[dataParts.shift()] = dataParts.join(':');
    });

    var id_parts = id.trim().split(':');
    if (id_parts.length >= 2) {
      var provider = id_parts.shift().trim();
      var termid = id_parts.join(':').trim();
      var idfix = termid.replace(' ', '').trim();
      var classes = [];
      classes.push(provider.toLowerCase());
      if (providerlabel) {
        classes.push(providerlabel.toLowerCase());
      }
      // Prevent duplicates under the same autocomplete field.
      if ($(this).parents('.yild__fieldset').find('.yild__termButton[data-id="' + termid + '"]').length === 0) {
        var buttonMarkup;
        var buttonHeader = '';
        var buttonContents = '';
        if (usage) {
          if (usage > 1000) {
            usage = Math.round(usage/100) / 10 + 'k';
          }
          buttonHeader += '<div class="yild__frequency yild__frequency--termbutton">' + usage + '</div>';
        }
        if (providerlabel || Drupal.settings.yild.yild_show_provider) {
          if (asContainer) {
            buttonHeader += '<div class="yild__provider yild__provider--autosuggest">' + (providerlabel ? Drupal.t(providerlabel) : provider) + '</div>';
          }
          else if (Drupal.settings.yild.yild_show_provider) {
            displaylabel = (providerlabel ? Drupal.t(providerlabel) : provider) + ': ' + displaylabel;
          }
        }
        buttonHeader += '<div class="yild__button--remove">&otimes;</div>';
        buttonHeader += '<span class="yild__termButtonLabel" title="' + displaylabel + (disambiguator.length > 0  ? ' (' + disambiguator + ')' : '') + '">' + displaylabel + (disambiguator.length > 0 && asContainer ? ' <span class="yild__disambiguator">(' + disambiguator + ')</span>' : '') + '</span>';
        buttonHeader = '<div class="yild__clearAfter">' + buttonHeader + '</div>';

        // Add long description and image if available.
        if ((longdescription || dataItems['image']) && asContainer) {
          if (dataItems['image']) {
            buttonContents += '<img class="yild__descriptionImage yild__descriptionImage--long" src="' + dataItems['image'] + '"/>';
          }
          buttonContents += '<span class="yild__description">' + longdescription + '</span>';
          buttonContents = '<div class="yild__termButtonLongdescription yild__clearAfter">'  + buttonContents + '</div>';
        }

        if ($(this).csvContainsValue(disambiguator, 'Person')) {
          classes.push('yild__termButtonPerson');
        }
        if ($(this).csvContainsValue(disambiguator, 'Place')) {
          classes.push('yild__termButtonPlace');
        }

        buttonMarkup = '<li class="yild__termButton ' +
          (inactive ? 'yild__termButton--inactive' : '') +
          ' ' + classes.join(' ') +
          '" id="' + provider + idfix + '" data-id="' + termid +
          '" data-provider="' + provider + '" data-label="' + label +
          '" data-disambiguator="' + disambiguator + '" data-data="' + data +
          '">' + buttonHeader + buttonContents + '</li>';

        $(this).append(buttonMarkup);
        $(this).yildRefreshButtons();
      }
    }
  };

  /**
   * Adds a term to all Yild fields using a specific provider.
   *
   * @param {int} id - Unique term id.
   * @param {string} label - Term label.
   * @param {string} disambiguator - Term disambiguator that describes what type of concept it is.
   * @param {array} data - Various data associated with the concept.
   * @param {element} hidden_field_origin - The jQuery element for a hidden_field.
   * @param {bool} inactive - Whether the term button should be rendered as inactive.
   */
  $.fn.yildAddTagToProviderField = function (id, label, disambiguator, data, hidden_field_origin, inactive) {
    var id_parts = id.trim().split(':');
    if (id_parts.length >= 2) {
      var provider = id_parts.shift().trim();
      $('input.' + provider).parents('.yild_fieldset').find('ul.yild_tags_container').each(function () {
        var hidden_field = $(this).parent().find('input.hidden_yild_field');
        $(this).addYildTag(id, label, disambiguator, null, data, null, null, inactive, hidden_field);
        $(this).yildMoveValues(hidden_field);
      });
    }
  };

  /**
   * Add values to the hidden fields from the container with term buttons.
   *
   * @param {element} hidden_field - The jQuery element for a hidden_field.
   */
  $.fn.yildMoveValues = function (hidden_field) {
    var id_list = [];
    $(this).find('li.yild__termButton').not('.yild__termButton--inactive').each(function () {
      // Push the id within double quotes, so we don't break it if the id
      // happens to contain a comma.
      // Also fix label and disambiguator that might contain quotes.
      var label = disambiguator = '';
      if ($(this).data('label')) {
        label = String($(this).data('label')).replace(/"/g, '&quot;');
      }
      if ($(this).data('disambiguator')) {
        disambiguator = String($(this).data('disambiguator')).replace(/"/g, '&quot;');
      }
      var idval = '"' + $(this).data('provider') + ':' + $(this).data('id') + '|' + label + '|' + disambiguator + '|' + $(this).data('data') + '"';
      id_list.push(idval);
    });
    hidden_field.val(id_list.join(','));
    $(this).yildRefreshProgressBar();
  };

  /**
   * Refresh all term button handlers, such as removing a term.
   *
   * This method is used on tag containers.
   */
  $.fn.yildRefreshButtons = function () {
    var container = $(this);
    var hidden_field = container.yildGetHiddenField();
    $(this).find('.yild__button--remove').each(function () {
      $(this).yildRefreshRemove(container, hidden_field);
    });
    $(this).find('.yild__termButton--inactive').each(function () {
      $(this).unbind('click');
      $(this).click(function () {
        $(this).unbind('click');
        $(this).removeClass('yild__termButton--inactive');
        var target_container = $(this).parent().parent().siblings('.js-yild-tags-container');
        if (target_container.find('#' + $(this).attr('id')).length === 0) {
          $(this).find('.yild__termButtonLongdescription, .yild__frequency, .yild__provider, .yild__disambiguator').remove();
          $(this).detach().appendTo(target_container);
          $(this).find('.yild__button--remove').yildRefreshRemove(target_container, hidden_field);
          target_container.yildMoveValues(hidden_field);
        }
        else {
          $(this).remove();
        }
      });
    });
    $('.yild__button--expandDescription').unbind("click");
    $('.yild__button--expandDescription').click(function() {
      $(this).parent().siblings('.yild__autosuggestSuggestionsContainer').find('.yild__termButtonLongdescription').slideToggle();
    });
  };

  /**
   * Refreshes the remove event handler for a given term button.
   *
   * @param {element} container - The container in which the removed term button is.
   * @param {element} hidden_field - The jQuery element for a hidden_field.
   */
  $.fn.yildRefreshRemove = function (container, hidden_field) {
    $(this).unbind('click');
    $(this).click(function () {
      $(this).parents('.yild__termButton').remove();
      if (!$(this).parents('.yild__termButton').hasClass('yild__termButton--inactive')) {
        container.yildMoveValues(hidden_field);
      }
    });
  };

  /**
   * If a progress bar exists for the ideal amount of terms, we show it and update it for the current situation.
   */
  $.fn.yildRefreshProgressBar = function () {
    var progressBar = $(this).siblings('.yild__progressBar');
    if (progressBar.length > 0) {
      var percentage = Math.round(100 * $(this).find('.yild__termButton').not('.yild__termButton--inactive').length / progressBar.data('amount'));
      if (percentage > 130) {
        percentage = 130;
      }
      var marked = false;
      $.each([100, 50], function (index, value) {
        progressBar.children('.bar').removeClass('bar-' + value);
        if (percentage >= value && !marked) {
          marked = true;
          progressBar.children('.bar').addClass('bar-' + value);
        }
      });
      progressBar.children('.bar').width(percentage + '%');
    }
  };

  /**
   * Returns the hidden field from within a container.
   *
   * @return {element} the hidden input field.
   */
  $.fn.yildGetHiddenField = function () {
    return $(this).parents('.yild__fieldset').find('.js-yild-hidden-field');
  };

  /**
   * This is called only on form validation error to repopulate tags.
   *
   * If a form is incorrectly filled out, this will be called to remake pill
   * buttons from the hidden field submitted with the wrongly filled out form.
   *
   * @param {element} hidden_field - The jQuery element for a hidden_field.
   */
  $.fn.yildMakeButtonsFromHiddenField = function (hidden_field) {
    var hiddenval = hidden_field.val();
    var button_parts;
    if (hiddenval.indexOf('|') !== -1) {
      // Explode strings at comma that may or may not be enclosed by double quotes.
      // See: http://stackoverflow.com/questions/11456850/split-a-string-by-commas-but-ignore-commas-within-double-quotes-using-javascript.
      var hiddenvals = hiddenval.match(/(".*?"|[^",\s]+)(?=\s*,|\s*$)/g);
      for (var h = 0; h < hiddenvals.length; h++) {
        // Remove enclosing quotes.
        hiddenvals[h] = hiddenvals[h].replace(/(^"|"$)/g, '');
        button_parts = hiddenvals[h].split('|');
        if (button_parts.length >= 3) {
          // Make sure array indeces are not empty.
          for (var i = 3; i <= 4; i++) {
            if (!button_parts[i]) {
              button_parts[i] = '';
            }
          }
          $(this).addYildTag(button_parts[0], button_parts[1], button_parts[2], false, button_parts[3], null, null, false, hidden_field);
        }
      }
    }
  };
})(jQuery);
