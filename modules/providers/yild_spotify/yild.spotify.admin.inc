<?php

/**
 * @file
 * Yild configuration UI.
 */

/**
 * Implements hook_admin_settings_form().
 */
function yild_spotify_admin_settings_form($form, &$form_state) {
  $form = array();

  $form['yild_spotify_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Yild Spotify configuration'),
    '#description' => t('To use this provider you have to <a href="@spotify_url">register an app</a> in Spotify to receive a client ID and a client secret key. These are used to request an access token that is used to access the Spotify Web API.', array('@spotify_url' => url('https://developer.spotify.com/my-applications'))),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['yild_spotify_settings']['yild_spotify_client_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Spotify Client ID'),
    '#description' => t('The Spotify Client ID for authenticating against the Spotify API.'),
    '#default_value' => variable_get('yild_spotify_client_id', ''),
    '#disabled' => FALSE,
    '#size' => 100,
    '#maxlength' => 100,
    '#required' => TRUE,
  );

  $form['yild_spotify_settings']['yild_spotify_client_secret'] = array(
    '#type' => 'textfield',
    '#title' => t('Spotify Client secret key'),
    '#description' => t('The Spotify Client secret key for authenticating against the Spotify API.'),
    '#default_value' => variable_get('yild_spotify_client_secret', ''),
    '#disabled' => FALSE,
    '#size' => 100,
    '#maxlength' => 100,
    '#required' => TRUE,
  );

  return system_settings_form($form);
}
