From b4f7a5203a28cc384a34602e7ba0ba2e6763a741 Mon Sep 17 00:00:00 2001
From: Antti Tuppurainen <antti.tuppurainen@industry62.com>
Date: Tue, 15 May 2018 15:09:46 +0300
Subject: [PATCH] Fix spotify provider and coding standards

---
 .../providers/yild_spotify/yild.spotify.admin.inc  | 45 +++++++++++
 modules/providers/yild_spotify/yild_spotify.info   |  1 +
 modules/providers/yild_spotify/yild_spotify.module | 89 +++++++++++++++++++++-
 3 files changed, 134 insertions(+), 1 deletion(-)
 create mode 100644 modules/providers/yild_spotify/yild.spotify.admin.inc

diff --git a/modules/providers/yild_spotify/yild.spotify.admin.inc b/modules/providers/yild_spotify/yild.spotify.admin.inc
new file mode 100644
index 0000000..7fc1e0d
--- /dev/null
+++ b/modules/providers/yild_spotify/yild.spotify.admin.inc
@@ -0,0 +1,45 @@
+<?php
+
+/**
+ * @file
+ * Yild configuration UI.
+ */
+
+/**
+ * Implements hook_admin_settings_form().
+ */
+function yild_spotify_admin_settings_form($form, &$form_state) {
+  $form = array();
+
+  $form['yild_spotify_settings'] = array(
+    '#type' => 'fieldset',
+    '#title' => t('Yild Spotify configuration'),
+    '#description' => t('To use this provider you have to <a href="@spotify_url">register an app</a> in Spotify to receive a client ID and a client secret key. These are used to request an access token that is used to access the Spotify Web API.', array('@spotify_url' => url('https://developer.spotify.com/my-applications'))),
+    '#collapsible' => TRUE,
+    '#collapsed' => FALSE,
+  );
+
+  $form['yild_spotify_settings']['yild_spotify_client_id'] = array(
+    '#type' => 'textfield',
+    '#title' => t('Spotify Client ID'),
+    '#description' => t('The Spotify Client ID for authenticating against the Spotify API.'),
+    '#default_value' => variable_get('yild_spotify_client_id', ''),
+    '#disabled' => FALSE,
+    '#size' => 100,
+    '#maxlength' => 100,
+    '#required' => TRUE,
+  );
+
+  $form['yild_spotify_settings']['yild_spotify_client_secret'] = array(
+    '#type' => 'textfield',
+    '#title' => t('Spotify Client secret key'),
+    '#description' => t('The Spotify Client secret key for authenticating against the Spotify API.'),
+    '#default_value' => variable_get('yild_spotify_client_secret', ''),
+    '#disabled' => FALSE,
+    '#size' => 100,
+    '#maxlength' => 100,
+    '#required' => TRUE,
+  );
+
+  return system_settings_form($form);
+}
diff --git a/modules/providers/yild_spotify/yild_spotify.info b/modules/providers/yild_spotify/yild_spotify.info
index 6d7067f..ad3b1e5 100644
--- a/modules/providers/yild_spotify/yild_spotify.info
+++ b/modules/providers/yild_spotify/yild_spotify.info
@@ -3,3 +3,4 @@ description = Provides Spotify integration for Yild
 package = Yild providers
 core = 7.x
 dependencies[] = yild
+configure = admin/config/yild/spotify
diff --git a/modules/providers/yild_spotify/yild_spotify.module b/modules/providers/yild_spotify/yild_spotify.module
index b381676..39e2872 100644
--- a/modules/providers/yild_spotify/yild_spotify.module
+++ b/modules/providers/yild_spotify/yild_spotify.module
@@ -5,10 +5,37 @@
  * Defines a Spotify provider for Yild.
  */
 
-define('YILD_SPOTIFY_API', 'http://api.spotify.com/v1');
+define('YILD_SPOTIFY_API', 'https://api.spotify.com/v1');
+define('YILD_SPOTIFY_ACCESS_TOKEN_URI', 'https://accounts.spotify.com/api/token');
 define('YILD_SPOTIFY_CACHE_LIFETIME', 86400);
 define('YILD_SPOTIFY_MAX_HITS', 10);
 
+/**
+ * Implements hook_menu().
+ */
+function yild_spotify_menu() {
+  $items = array();
+
+  $items['admin/config/yild/spotify'] = array(
+    'title' => 'Yild Spotify settings',
+    'description' => 'Settings for Yild Spotify provider.',
+    'page callback' => 'drupal_get_form',
+    'page arguments' => array('yild_spotify_admin_settings_form'),
+    'access arguments' => array('administer site configuration'),
+    'file' => 'yild.spotify.admin.inc',
+    'type' => MENU_NORMAL_ITEM,
+  );
+  return $items;
+}
+
+/**
+ * Implements hook_enable()
+ * Show a message about necessary configuration that is needed before this provider can be used.
+ */
+function yild_spotify_enable() {
+  drupal_set_message(t('Please <a href="@configure">configure</a> this provider before using it.', array('@configure' => url('admin/config/yild/spotify'))), 'status');
+}
+
 /**
  * Implements hook_yild_search().
  *
@@ -28,6 +55,7 @@ function yild_spotify_yild_search($search_string, $lang = 'en') {
         'method' => 'GET',
         'timeout' => 3,
         'headers' => array(
+          'Authorization' => 'Bearer ' . _yild_spotify_get_access_token(),
           'Accept' => 'application/json',
         ),
       );
@@ -70,6 +98,65 @@ function yild_spotify_yild_get_provider_name() {
   return 'spotify';
 }
 
+/**
+ * Checks if the Spotify access token exists in cache. If not, fetch a new one.
+ *
+ * @return string
+ *   The access token.
+ */
+function _yild_spotify_get_access_token() {
+  $access_token = &drupal_static(__FUNCTION__);
+
+  if(!isset($access_token)) {
+    if($cache = cache_get('yild:spotify:access_token', 'cache')) {
+      $access_token = $cache->data;
+    }
+    else {
+      $access_token = _yild_spotify_fetch_access_token();
+      cache_set('yild:spotify:access_token', $access_token, 'cache', time() + 3600);
+    }
+  }
+  return $access_token;
+}
+
+/**
+ * Get the access token from Spotify Web API
+ *
+ * @return string
+ *   The access token from Spotify Web API.
+ */
+function _yild_spotify_fetch_access_token() {
+  $payload = drupal_base64_encode(variable_get('yild_spotify_client_id') . ":" . variable_get('yild_spotify_client_secret'));
+
+  $parameters = array(
+    'grant_type' => 'client_credentials'
+  );
+
+  $query = http_build_query($parameters);
+
+  $options = array(
+    'method' => 'POST',
+    'data' => $query,
+    'headers' => array(
+      'Authorization' => 'Basic ' . $payload,
+      'Content-Type' => 'application/x-www-form-urlencoded',
+    ),
+  );
+
+  $result = drupal_http_request(YILD_SPOTIFY_ACCESS_TOKEN_URI, $options);
+
+  if (empty($result->error)) {
+    $token_data = drupal_json_decode($result->data);
+    if(!empty($token_data['access_token'])) {
+      $access_token = $token_data['access_token'];
+      return $access_token;
+    }
+  }
+  else {
+    return FALSE;
+  }
+}
+
 /**
  * Retrieves a search result from Drupal's internal cache.
  *
-- 
2.14.3 (Apple Git-98)

